\chapterspecial{Posfácio para uma Trilogia}{}{}
 

\section{O \versal{TREM}, O \versal{ENTRE} E O}

\section{\emph{\versal{PARADISO}} \versal{TERRESTRE}\footnoteInSection{Texto publicado na revista \emph{Literatura e Sociedade}, n.8 em 2005.} }

\begin{flushright}\emph{Ao Teruo, que saltou e se abrigou}\end{flushright}
 \begin{flushright}\emph{no antes da partida}\end{flushright}


Estamos hoje, quase todos nós, confinados dentro de um trem que corre
para frente em velocidade cada vez mais acelerada e vertiginosa.
Impossível procurar pelo maquinista a fim de controlar o curso ou
deliberar acerca da rota do trem. Nos últimos 50 anos, já não há mais
maquinista e o trem avança sozinho como um míssil de alcance ilimitado.
Afirmar que a meta desse trem é encontrar a explosão e a catástrofe
``reais'' enco bre a experiência mais sutil e mais decisiva de que a
catástrofe já se instalou, pois o brilho e o rosto da in tensidade
humana já não podem florescer nem circu lar na ambiência e na
arquitetura das cabines situadas no interior do trem. O~homem que
compreendeu a raiz da sua dor após ter sofrido em si mesmo e em seu pró
prio corpo o apagamento do brilho e a aniquilação da intensidade
torna"-se uma espécie de artista e, quer ele escreva, quer ele não
escreva, a simplicidade de sua tarefa se resume em transitar no interior
do trem e, em todas as cabines e compartimentos, perguntar a cada um que
encontra: ``Você não tem saudade de ir para o lugar fora do trem?'' ``De
quão longe você vem?'' ``Quan do você era ainda uma criança, em que
espaço você estava?'' ``Você não gostaria de rememorar como eram as
coisas antes do ponto de embarque?''. Estas quatro perguntas"-gêmeas
abrigam toda a ``força'' da arte e são perguntas proferidas na direção
contrária à da multi dão de artistas que apenas decoram as cabines do
trem com imagens mortas e textos competentes. Uma primeira coisa,
portanto, protege o abrigo da arte: a potên cia do estranhamento. Nela o
interior do trem é experi mentado como exílio e falta de lugar, pois a
dor própria ao estranhamento comunica ao homem que ele não pertence ao
espaço medido e saturado do interior do trem. O~estranhamento é um afeto
de passagem e o seu ``não'' para os compartimentos do trem retém, incuba
do, o ``sim'' para uma outra pertença, a do lugar aberto e destituído de
medida onde a jovialidade e a celebra ção podem acontecer. O~culto ao
estranhamento, hoje tão em moda nos meios psicanalíticos e acadêmicos,
prende numa moldura a turbulência do negativo, im"-pedindo que ele
realize sua tarefa até o fim, pois, como já disse, o estranhamento é
passagem, é desejo de par tir, e, uma vez experimentado até o fundo,
rasga a arga massa metálica do trem e nos coloca sentados no for
migueiro da incandescência, no cometa onde a criança enlouquece de
lucidez. E é precisamente nalgum rasgo ou buraco do trem, entre a terra
redimida e o mundo instituído, que mora o escritor contemporâneo: ele é
o mediador com cabeça de Jano. Se um olho olha para dentro desse trem
que começa a sair do solo na direção da imortalidade biológica e da
conquista cósmica, o outro olho, o da nuca, olha para o pedaço de praia
onde uma criança acaba de levantar os braços inda gando a aparição de um
caranguejo. Dizer o que vê e o que sente- estando dilacerado entre a
criança"-habi tante (jamais a criança inventada da psicologia e da
psicanálise!), a criança"-visitada de poucas palavras e o adulto atual,
preenchido de palavras por todos os lados, mesmo porque, plantado direto
no logos e já destituído de qualquer experiência- tal é, a meu ver, a
tarefa e a posição do escritor atual. Posição ambígua e difícil a do
espião de umbral. Para dar uma ideia dessa posição, é necessário
imaginar um alfandegário da úl tima fronteira e formular"-lhe a seguinte
pergunta: ``Mas como é que passam por aqui os que aqui passam?''. E~ele
responderia: ``Passam sempre conduzidos pela dor, pois os que vêm do
aberto, quando precisam circular no trem, têm de vestir uma máscara que
os impede de respirar, e os normalizados"-do"-trem, quando se diri gem
para o simples, têm de deixar no vagão todo o uni forme da sua
identidade. Eles só passam para lá nus''. O~escritor, como esse
alfandegário no país derradeiro, é aquele que está em condições de
conversar tanto com o poeta obscuro, que só tem o olho da nuca, quanto
com o homem sentado no compartimento, homem cujo gesto e cujo corpo já
mimetizaram a blindagem do trem e cujo olho, hipnotizado, olha apenas
para frente. Ele, o escritor, é o que tenta fazer a mediação entre dois
idiomas intraduzíveis; daí o caráter trágico da sua si tuação. Quando
ele senta no compartimento do trem, munido de sua máscara respiratória,
que bem pode ser o rótulo e a identidade imaginária de escritor, então
ele o faz para cutucar o companheiro de cadeira e para indagá"-lo: ``Ei
moço, ei senhor, mas como pode ser tão grande o apelo desse trem?''. ``O
senhor não está perce bendo que está incômodo por aqui?''. Se até os
anos 20 ou 30 (tempo de Robert Walser, de Kafka ou de Pessoa) o trem era
ainda uma locomotiva, uma espécie de ``maria fumaça'' com algum espaço
nas cabines, a partir dos anos 50 (Gombrowicz, Bernhard, Imre Kertész e
Handke) ele começa a se transformar num trem"-bala e tudo em seu interior
vai se comprimindo num incesto gigantes co, de tal modo que a dor se
agudiza e o estranhamento dá lugar ao pavor e ao horror. Se o pavor
ainda não é experimentado com toda transparência pelo homem no
compartimento, é porque o seu olho"-de"-trás dorme e está atrofiado.
Aumentá"-lo e acordá"-lo é a tarefa da arte: cabe a ela conversar com a
nostalgia e o pressentimen to adormecidos no homem. Se ela pode fazer
isso, é porque é mensageira; porque sua palavra, ainda que proferida no
interior do trem, foi colhida alhures, no lugar sem arranjos onde a
única medida é a amizade pelas coisas e pelos outros. Esse lugar não é o
produto de uma imaginação utópica, mas para onde somos ar rastados
sempre e quando o regime da aparência dá lugar à vibração do surgimento.

Os três livros que publiquei não são mais do que o relato"-recolhimento
de uma experiência de passagem. Não há neles criação nem imaginação.
Apenas escrevi no papel o que antes se inscreveu em mim. Penso que um
texto pode se tornar texto se ele é a expressão e o idioma íntimo do
destino de seu autor. Se, ao contrá rio, o autor ``buscou um tema'' ou
``teve diante de si um objeto'', então ele já estava fora da
possibilidade de um conhecimento efetivo. Sabedoria do Nunca, o primeiro
livro da trilogia, é uma síntese dos diários que escrevi de 1983 até
1994. Neles ruminei minha falta de lugar e a oscilação dolorosa entre o
desejo de pertencer ao trem e o terror de um dia fazer parte dele. Eram
o estranha menta e a dor uma doença psiquiátrico"-psicológica ou
delatavam o caráter claustrofóbico das cabines? Impe dido de ultrapassar
esta indecisão, o livro afirma a dor, o negativo e o suicídio como os
abrigos diante do ofuscamento do trem. O~segundo livro, Ignorância do
Sempre, é um livro de transição. Nele a dOr já não noti cia o fracasso
em obter lugar nos assentos do trem, mas começa a aparecer como a
protetora e a guardiã do olho da nuca. No terceiro livro, Certeza do
Agora, o olho da nuca já está bem aberto, já olhou para dentro da di
mensão real e é a partir de lá que desmascara e ultra passa o regime que
vigora nos compartimentos. Ali já não há mais dúvida: o homem sem o olho
de trás é um títere do trem e a vida nos compartimentos é apenas a
execução de um roteiro predisposto pela própria orga nização do trem,
organização essa que destrói e blo queia o surgimento do rosto humano.

\section{\versal{OUTRAS} \versal{REFER}Ê\versal{NCIAS}}

\begin{itemize}
\item
  \versal{ADORNO}, T\,\emph{Mínima Moralia}. São Paulo, Ática, 1992.
\item
  \_\_\_\_\_\_\_\_. \emph{Notas de Literatura}. Rio de Janeiro, Tempo
  Brasileiro, 1991.
\item
  \versal{AKHMÁTOVA}, A\,\emph{Poesia}. Porto Alegre, L\&\versal{PM}, 1991.
\item
  \versal{BAUDRILLARD}, J\,\emph{A~Ilusão Vital}. Rio de Janeiro, Civilização
\item
  Brasileira, 2001.
\item
  \_\_\_\_\_\_\_\_. \emph{A~Sociedade de Consumo}. Lisboa, Edições 70,
  1995.
\item
  \_\_\_\_\_\_\_\_. \emph{À Sombra das Maiorias Silenciosas --- O Fim do
  Social e o Surgimento das Massas}. São Paulo, Brasiliense, 1994.
\item
  \_\_\_\_\_\_\_\_. \emph{A~Transparência do Mal --- Ensaios sobre os
  Fenômenos Extremos}. Campinas, Papirus, 1992.
\item
  \versal{BERNHARD}, T\,\emph{Extinção}. São Paulo, Companhia das Letras, 2000.
\item
  \_\_\_\_\_\_\_\_. \emph{Trevas}. Lisboa, Hiena, 1993.
\item
  \versal{BLANCHOT}, M\,\emph{Apres Coup- Précédé par le Ressassement Éternel}.
  Paris, Les Éditions de Minuit, 1983.
\item
  \_\_\_\_\_\_\_\_. \emph{El Paso (no) Más Allá}. Barcelona, Paidós,
  1994.
\item
  \_\_\_\_\_\_\_\_. \emph{La Risa de los Dioses}. Madrid, Taurus, 1976.
\item
  \_\_\_\_\_\_\_\_. \emph{L'entretien Infini}. Paris, Gallimard, 1992.
\item
  \_\_\_\_\_\_\_\_. \emph{L'espace Littéraire}. Paris, Gallimard, 1988.
\item
  \_\_\_\_\_\_\_\_. \emph{Lautréamont et Sade}. Paris, Les Éditions de
  Minuit, 1984.
\item
  \versal{CELAN}, P\,\emph{Arte Poética --- O Meridiano e outros Textos}. Lisboa,
  Cotovia, 1996.
\item
  \_\_\_\_\_\_\_\_. Choix de Poihnes \emph{---} Rézmis par L'auteur.
  Paris, Gallimard, 1998.
\item
  \_\_\_\_\_\_\_\_. \emph{Cristal}. São Paulo, Iluminuras, 1999.
\item
  \_\_\_\_\_\_\_\_. \emph{Sete Rosas Mais Tarde}. Lisboa, Cotovia, 1993.
\item
  \versal{CLORAN}, E\,M\,De L'inconvénient D'être Né. Paris, Gallimard, 1990.
\item
  \_\_\_\_\_\_\_\_. E/ Aciago Demiurgo. Madrid, Taurus, 1979.
\item
  \_\_\_\_\_\_\_\_. Ese Maldito Yo. Barcelona, Tusquets, 1988.
\item
  \_\_\_\_\_\_\_\_. La Chute dans /e Temps. Paris, Gallimard, 1987.
\item
  \_\_\_\_\_\_\_\_. Exercícios de Admiração. Rio de Janeiro, Guanabara,
  1988.
\item
  \_\_\_\_\_\_\_\_. La Tentation D'exister. Paris, Gallimard, 1995.
\item
  \_\_\_\_\_\_\_\_. Précis de Décomposition. Paris, Gallimard, 1987.
\item
  \_\_\_\_\_\_\_\_. Silogismos da Amargura. Rio de Janeiro, Rocco, 1991.
\item
  \_\_\_\_\_\_\_\_. Storia e Utopia. Milano, Edizioni del Borghese,
  1969.
\item
  \versal{DEBORD}, G\,A~Sociedade do Espetáculo. Lisboa, Afrodite, 1972.
\item
  \versal{DELEUZE}, G\,Crítica e Clínica. São Paulo, Editora 34, 1997.
\item
  \_\_\_\_\_\_\_\_. Foucault. São Paulo, Brasiliense, 1988.
\item
  \_\_\_\_\_\_\_\_. Lógica do Sentido. São Paulo, Perspectiva, 1994.
\item
  \versal{DELEUZE}, G\,\& \versal{GUATTARI}, F\,O~que é Filosofia? Rio de Janeiro, Editora
  34, 1997.
\item
  \_\_\_\_\_\_\_\_. Mil Platôs. Rio de Janeiro, Editora 34, 1995, vol.
  1.
\item
  \versal{DELEUZE}, G\,\& \versal{PARNET}, C\,Diálogos. São Paulo, Escuta, 1998.
\item
  \versal{DOSTOIÉVSKI}, F\,Crime e Castigo. Lisboa, Minerva, 1974, vol. I e \versal{II}.
\item
  \_\_\_\_\_\_\_\_. Notas do Subterrâneo. Rio de Janeiro, Civilização
  Bra sileira, 1986.
\item
  \versal{FOUCAULT}, M\,Doença Mental e Psicologia. Rio de Janeiro, Tempo
  Brasileiro, 1984.
\item
  \versal{GOMBROWICZ}, W\,Bakakai. Rio de Janeiro, Expressão e Cultura, 1968.
\item
  \_\_\_\_\_\_\_\_. Cosmos. Paris, Folio, 1989.
\item
  \_\_\_\_\_\_\_\_. Curso de Filosofia en Seis Horas y Cuarto.
  Barcelona, Tusquets, 1990.
\item
  \_\_\_\_\_\_\_\_. Journal Paris Berlin. Paris, Christian Bourgofs
  Éditeur, 1968.
\item
  \_\_\_\_\_\_\_\_. Ferdydurke. Buenos Aires, Sudamericana, 1983.
\item
  \_\_\_\_\_\_\_\_. Gombrowicz --- Dubuffet Correspondencia. Barcelona,
  Anagrama, 1972.
\item
  \_\_\_\_\_\_\_\_. La Pornografie. Paris, Christian Bourgois Éditeur,
  1990.
\item
  \_\_\_\_\_\_\_\_. Morte ao Dante. Lisboa, \& Etc, 1987.
\item
  \_\_\_\_\_\_\_\_. Peregrinaciones Argentinas. Madrid, Alianza
  Editorial, 1987.
\item
  \_\_\_\_\_\_\_\_. Testamento. Barcelona, Anagrama, 1991.
\item
  \_\_\_\_\_\_\_\_. Transatlántico. Barcelona, Anagrama, 1986.
\item
  \_\_\_\_\_\_\_\_. Varia \versal{II}. Paris, Christian Bourgois Éditeur, 1989.
\item
  \versal{HAAR}, M\,Heidegger e a Essência do Homem. Lisboa, Instituto Piaget,
  s.Id.
\item
  \versal{HANDKE}, P\,A~Ausência. Rio de Janeiro, Rocco, 1989.
\item
  \_\_\_\_\_\_\_\_. História de uma Infância. São Paulo, Companhia das
  Letras, 1990.
\item
  \_\_\_\_\_\_\_\_. A~Tarde de um Escritor. Rio de Janeiro, Rocco, 1993.
\item
  \versal{HEIDEGGER}, M\,Caminos de Bosque. Madrid, Alianza Editorial, 2000.
\item
  \_\_\_\_\_\_\_\_. Estudios sobre Mística Medieval. México, Fondo de
  Cul tura Económica, 1997.
\item
  \_\_\_\_\_\_\_\_. Heráclito. Rio de Janeiro, Relume Dumará, 2000.
\item
  \_\_\_\_\_\_\_\_. Kant y e{[} Problema de la Metafísica. México, Fondo
  de Cultura Económica, 1981.
\item
  \_\_\_\_\_\_\_\_. Qu'appelle"-t-on Penser? Paris, \versal{PUF}, 1992.
\item
  \_\_\_\_\_\_\_\_. La ``Phénoménologie de l'esprit'' de Hegel. Paris,
  Gallimard, 1984.
\item
  \_\_\_\_\_\_\_\_. La Proposición de/ Fundamento. Barcelona, Odós,
  1991.
\item
  \_\_\_\_\_\_\_\_. ``A Sentença de Anaximandro'' e ``Lagos e
  Alétheia''. Pré"-Socráticos. São Paulo, Abril Cultural (Os Pensadores),
  1978.
\item
  \_\_\_\_\_\_\_\_. ``Sobre o `Humanismo''' e ``O Fim da Filosofia e a
  Tarefa do Pensamento''. Heidegger. São Paulo, Abril Cul tural (Os
  Pensadores), 1979.
\item
  \_\_\_\_\_\_\_\_. Traité des Catégories et de la Signification: Paris,
  Gallimard, 1970.
\item
  \versal{HELDER}, H\,Os Passos em Volta. Lisboa, Assírio \& Alvim, 1997.
\item
  \_\_\_\_\_\_\_\_. Photomaton \& Vox. Lisboa, Assírio \& Alvim, 1987.
\item
  \versal{HOOCE}, J\,Heidegger e a Ética. Lisboa, Instituto Piaget, s./d.
\item
  HÖ\versal{LDERLIN}, F\,Elegias. Lisboa, Assírio \& Alvim, 1992.
\item
  \_\_\_\_\_\_\_\_. Oeuvres. Paris, Gallimard, 1967.
\item
  \_\_\_\_\_\_\_\_. Poemas. Coimbra, Atlântida, 1959.
\item
  \_\_\_\_\_\_\_\_. Reflexões. Rio de Janeiro, Relume Dumará, 1994.
\item
  \versal{JUARROZ}, R\,Decimocuarta Poesía Vertical. Fragmentos Vertica/es.
  Buenos Aires, Emecé, 1997.
\item
  \_\_\_\_\_\_\_\_. Poesía Vertica/1983/1993. Buenos Aires, Emecé, 1997.
\item
  \versal{JÜNGER}, E\,Der Arbeiter --- Herrschaft und Gestalt. Stuttgart, Cotta's
  Bibliothek der Moderne, 1981.
\item
  \_\_\_\_\_\_\_\_. Eumeswil. Rio de Janeiro, Guanabara, 1987.
\item
  \_\_\_\_\_\_\_\_. O~Problema de Aladino. Lisboa, Cotovia, 1989.
\item
  \_\_\_\_\_\_\_\_. Traité du Rebel/e ou /e Recours aux Forêts. Paris,
  Christian Bourgois Éditeur, 1981.
\item
  \versal{KAFKA}, F\,Um Artista da Fome ..A Construção. São Paulo, Companhia das
  Letras, 1998.
\item
  \_\_\_\_\_\_\_\_. O~Castelo. São Paulo, Companhia das Letras, 2000.
\item
  \_\_\_\_\_\_\_\_. Das Urteil. Frankfurt am Main, Fischer Taschenbuch
  Verlag, 1989.
\item
  \_\_\_\_\_\_\_\_. O~Veredito. Na Colônia Penal. São Paulo, Companhia
  das Letras, 1998.
\item
  \versal{MARCUSE}, H\,A~Ideologia da Sociedade Industrial. Rio de Janei ro,
  Zahar, 1979.
\item
  \versal{MICHAUX}, H\,Épreuves, Exorcismes. Paris, Gallimard, 1973.
\item
  \versal{MUSIL}, R\,L'homme sans Qualités. Paris, Éditions du Seuil, 1995, tome
  1, tome 2.
\item
  \_\_\_\_\_\_\_\_. O~Jovem Torless. Rio de Janeiro, Nova Fronteira,
  1981.
\item
  \versal{NIETZSCHE}, F\,Além do Bem e do Mal. São Paulo, Companhia das Letras,
  1992.
\item
  \_\_\_\_\_\_\_\_. Así Hab/o Zaratustra. Madrid, Alianza Editorial,
  1999.
\item
  \_\_\_\_\_\_\_\_. Crepúsculo dos Ídolos. Lisboa, Edições 70, s./d.
\item
  \_\_\_\_\_\_\_\_. Ecce Homo. São Paulo, Companhia das Letras, 1995.
\item
  \_\_\_\_\_\_\_\_. Genealogia da Moral. São Paulo. Companhia das Le-
  tras, 1998.
\item
  \_\_\_\_\_\_\_\_. Humano, Demasiado Humano. São Paulo, Companhia das
  Letras, 2000.
\item
  \_\_\_\_\_\_\_\_. O~Nascimento da Tragédia. São Paulo, Companhia das
  Letras, 1996.
\item
  \_\_\_\_\_\_\_\_. O~Anticristo. Lisboa, Edições 70, s./d.
\item
  \_\_\_\_\_\_\_\_. Poemas. Coimbra, Centelha, 1986.
\item
  \versal{PESSOA}, F\,Livro do Desassossego. Campinas. Editora da Unicamp, 1994,
  vol. I e \versal{II}.
\item
  \_\_\_\_\_\_\_\_. Corresponçlência -1905--1922. Lisboa, Assírio \&
  Alvim, 1999.
\item
  \_\_\_\_\_\_\_\_. Poemas Ingleses. Lisboa, Imprensa Nacional- Casa da
  Moeda, 1997, tomo li.
\item
  \versal{SCHULZ}, B\,Sanatório. Rio de Janeiro, Imago, 1994.
\item
  \_\_\_\_\_\_\_\_. Las Tiendas de Colar Canela. Barcelona, Barral,
  1972.
\item
  \versal{SHÜRMANN}, H\,Heidegger on Being and Acting: from Principies to
  Anarchy. Bloomington, Indiana University Press, 1990.
\item
  \versal{SLOTERDIJK}, P\,Extrafiamiento de/ Mundo. Valencia, Pre"-Textos, 1998.
\item
  \_\_\_\_\_\_\_\_. Mobilização Copernicana e Desarmamento Ptolomaico.
  Rio de Janeiro, Tempo Brasileiro, 1992.
\item
  \_\_\_\_\_\_\_\_. Regras para o Parque Humano. São Paulo, Estação
  Liberdade, 2000.
\end{itemize}
