\epigraph{À minha mãe, que me deu o essencial: solidão e exílio}{} 

\cleardoublepage{} 

\epigraph{Onde eu ontem dormi é hoje dia de descanso. Em frente da porta estão
empilhadas as cadeiras e nenhuma das pessoas a quem pergunto por mim me
viu.

Thomas Bernhard}{Thomas Bernhard} 

\epigraph{São horas talvez de eu fazer o único esforço de eu olhar para a minha
vida. Vejo"-me no meio de um deserto imenso. Digo do que ontem
literariamente fui, procuro explicar a mim próprio como cheguei aqui.
Fernando Pessoa}{Fernando Pessoa} 

\part{Ensaio} 

\epigraph{Vivo como o cuco no relógio não invejo os pássaros no bosque. Esta
missão me foi dada e eu canto. Sabe, destino semelhante, só a um inimigo
poderia desejá"-lo.
Anna Akhmátova}{Anna Akhmátova} 

\epigraph{}{} 

\chapter{\versal{PROVÍNCIA} \versal{DA} \versal{ESCRITURA}}

\begin{flushright}\emph{Ao José Feres, pois nome e assinatura emergem na terra da
amizade.}\end{flushright}


A vida de um homem\footnote{Este texto foi lido em 20 de março de 2001, na Livraria Cultura, no
evento denominado ``Francofonia, Lusofonia, Multifonia'', respondendo à
questão: O que é a língua para um escritor?, e posteriormente publicado
na Revista Cult, n.48.}  é o instante onde o mundo, em
vão, se ilumina. A~pedra, a lua e o rosto do outro não seriam
comemorados e celebrados se o breve trânsito de nossa aparição não
contasse com a língua e com a palavra. Também os gestos ou a dança, e a
pintura, igualmente celebram, mas é na palavra das línguas que o mundo
deixa de ser mudo e pode tocar a aparição. Se o homem deixar de existir
e apenas o lagarto ou outro animal grunhir para a lua, então ela será
menos lua e algum deus criador que acaso persista em sua incansável
persistência terá de reconhecer que sua ``obra'' não é mais devidamente
celebrada e ele, junto de seu imenso narcisismo trabalhista, teria de se
suicidar. Celebrar é estar exposto e atingido pelas coisas a ponto de,
ao dizê"-las, guardar"-lhes a vibração, comemorá"-las. Estar atingido
também pela proximidade do rosto do outro é enxergá"-lo a partir do
aberto, não sendo o aberto mais do que o lugar de uma aparição
intrinsecamente frágil, de uma aparição"-desaparição. Mas o homem não
gosta de estar exposto; ele é alérgico ao lugar. E, enquanto alérgico,
converteu"-se num animal blindado e, quando alguém aponta para o céu
(como um personagem de Bernhard) e diz: ``veja, ali está aberto, vejam,
está aberto; a palavra aberto está redigida no firmamento'', então, já
não se percebe o que isso quer dizer, pois o homem blindado gosta de
viver no fechado e de medir a palmo. O~homem blindado expulsou a
hospedagem: não está aberto à visitação dos afetos ou da palavra. O~blindado móvel em design estético é o homem moderno e ele é o ápice de
um fechamento que o fecha inteiramente em si mesmo e sobre si mesmo e já
não há espaço para a visitação do afeto ou para o jorro da língua, mas
apenas para vivências autofabricadas e auto"-afetadas. Vivências e
expressões vitais desencadeiam"-se por toda parte inteiramente criadas,
regradas e submetidas ao controle do ``sujeito'' que as cria. O~mesmo se
dá com as palavras, pois delas também se diz que são artefatos humanos,
de tal maneira que tanto o homem converteu"-se em artefato humano quanto
o enigma que nos instaura também converteu"-se em artefato humano.
Estamos, portanto, no miolo de uma tautologia insípida e estéril, pois
em toda parte ouvimos dizer o mesmo e ouvimos dizer que tudo é obra e
constructo humanos de tal maneira que superamos, já há muito, o
narcisismo obreiro da própria divindade. Já o trazemos em nosso
umbigo"-artefato, que em tudo difere do umbigo da terra e do umbigo da
nossa orfandade irrespondível. Por toda parte o homem tornou"-se um
teatro hiperconstruído e, quer vamos a um congresso dito psicanalítico
ou a um congresso dito filosófico, escutamos que a angústia é um
constructo do homem e que a palavra é um constructo do homem e que basta
trocar de metáforas que construiremos um novo constructo"-homem e,
logo"-logo, já ouvimos dizer que também a morte é um constructo do homem,
pois ela já foi equacionada em termos biotecnológicos. E~foi por ter
nascido no meio dessa tautologia insípida e por ter me horrorizado bem
no centro e no cerne dessa tautologia do espelhismo blindado que
refugiei"-me na província da escritura enquanto província de uma espera e
de um pressentimento. Eu vivi, portanto, apenas de diário e no diário, e
isso não foi de minha escolha nem de minha vonta de, pois vontade é uma
coisa tísica que diz respeito ao registro do sorvete e escolhas, aquelas
que contam, foram feitas quando tínhamos os olhos vendados, quando ainda
não tínhamos palavra e nem sabíamos o que escolher significava. Eu era
então uma criança e essa criança gostava de rasgar a boca das primas,
puxá"-las de um lado e do outro até dilacerar. Fazia o mesmo com o tio do
bigode, com o bigode do tio. Puxava um para cada lado. O~tio do bigode
era um homem movido a notícias, um homem abastecido por notícias e um
tio que morava inteiramente na terra das notícias. Eu puxava o seu
bigode, um para cada lado, até ele gritar. Eu levei 30 anos para
entender estes gestos, para saber qual o sopro que me soprava. Eu levei
aproximadamente 30 anos para me amigar da criança exacerbada, da criança
enlouquecida e me apropriar do segredo da escrita: dilacerar o homem,
reconduzi"-lo ao lugar, ao lugar onde jorrou o primeiro rosto contra a
noite da ausência. Dilacerar o homem é, portanto, celebrá"-lo e
reconduzi"-lo ao que lhe é próprio, ao acontecimento anterior às
construções. Por isso, quem escreve enquanto escrevo é a criança
exacerbada que rasgava a boca das primas e eu escrevo a reboque da
criança precisamente porque ela não é um constructo, ela não é obra de
ficção ou de artifício. Não se trata mais de auto"-superação
nietzscheana, mas de descida até a pobreza e a indigência para dentro do
elemento da alteridade que nos fundou. Contra toda invenção, um bocado
de confiança naquilo que é, pois aquilo que é emerge na terra da
destituição e a destituição vale o ouro, pois ela aponta na direção do
confiável. O~homem blindado, entretanto, já não confia em mais nada, nem
na vida nem na morte, e quando acontece de vida ou morte visitá"-lo ele
diz estar sofrendo da síndrome do pânico e busca então uma focinheira
química junto do psiquiatra. Ali no consultório eles conversam muito,
mas aquelas palavras"-de"-serviço já não celebram acontecimento algum a
não ser o negócio da administração da vida. E~foi por ter nascido num
tempo assim, repleto de palavras mortas, num tempo já sem nenhuma
herança ou tradição cultural, pois herança e tradição cultural são
pequenas dicas para que o homem transite e atravesse o aberto, dicas que
sussurram desde cedo --- isto é ter corpo, isto é comer, isto é morrer
---, dicas que surgem como respostas furadas à orfandade irrespondível,
dicas que eu, enquanto nascido no auge da modernidade mais moderna, não
cheguei a escutar, pois as palavras por mim escutadas diziam apenas
acerca de um subir na vida, de um arranjar profissão e de um tornar"-se,
logo"-logo, uma figura, sobretudo uma figura luzente e interessante,
destinada ao consumo das coisas e dos outros, pois é um vencedor
bem"-sucedido quem consome mais lugares e mais pessoas, numa busca
desenfreada e vertiginosa de intensificação e de hiperbolização do
vivido e, tendo eu próprio escutado essas palavras e tendo me parecido
que essas palavras eram na verdade destinadas mais a cupins e a formigas
que se nutrissem sistematicamente de uns e de outros, busquei a terra da
escritura e da palavra heterodoxa enquanto terra de uma retaguarda e de
uma espera. Foi apenas por que eu sofri formidavelmente na terra das
figuras, na terra do homem"-diploma e do homem"-doutor, na terra da
mulher"-charme e do homem"-mulher interessante e sensível, bem como na da
mulher eficaz e profissional que eu, não tendo encontrado aí eco ou
ressonância alguma e lugar algum de descanso e acolhimento, dirigi"-me
aos diários a fim de não ser triturado pela ofuscação gelada dos homens
blindados. Vale dizer que eu já, desde pequenininho, encontrei"-me em
contradição e numa situação completamente assimétrica em relação aos
lugares e pessoas que encontrei e, aos 19 anos, enquanto andava de
madrugada pelo meu bairro, um bairro infinitamente triste, com uma
bicicleta azul do tipo caloi em minhas costas, e eu gostava de levá"-la
nas costas até que aros e catracas me ferissem e eu então começava a
simular o ato de pedir carona e, diante do fato que ninguém parava eu
formulei, pela primeira vez, uma equação que ainda permanece válida, a
saber, que um homem com história, um homem protegido pela sacrossanta
identidade é como um camarada que tem carro e sabe de onde vem e quer ir
para algum lugar e nunca se desvia nem dá carona pra ninguém. E~eu
pensei, então, numa palavra intrinsecamente apocalíptica e visceral, uma
palavra que sabotasse essa imensa corporação de egoístas a fim de abrir
as brechas por onde passaria o milagre do evento, e essa palavra era
então uma palavra"-e-uma"-língua"-de"-ladrão"-de"-carros, uma palavra que
desmaterializasse o longo parênteses de ilusão para onde os homens se
enfiam na busca do abrigo e do torpor, e essa era uma palavra em mim
congênita e absolvida de qualquer esforço, pois eu já estava habituado a
olhar os homens em seus carros sempre a partir do pé e da bicicleta. E~foi junto da mesma bicicleta azul, durante uma incursão desolada e
repleta de pressentimento pelos bairros de Vila Gomes e do Rio Pequeno,
que eu, ao cruzar uma pequena ponte, uma quase"-pinguela, notei que ela
estava esburacada e cheia de rachaduras e que os homens"-carro a
atravessavam rapidamente sem olhar para o chão esburacado e eles a
cruzavam como se estivessem numa avenida e então, nessa noite,
compreendi que homem quer dizer Esquecimento. E~eu compreendi, então,
aquilo que a língua árabe já havia compreendido há muito tempo, a saber,
que a palavra homem quer dizer Esquecimento e que a palavra da escritura
é a palavra que despenca. A~palavra despencada é a palavra reminiscente,
pois nela o homem dá testemunho do surto"-susto de sua aparição e nela o
homem se aninha no lugar do assombro. A~língua da escritura é a língua
da palavra despencada e a palavra despencada desdiz a palavra
industrializada, a palavra cultivada e a palavra prostituída. Não há
negociação alguma nem mútua cooptação entre a palavra normalizada e a
palavra que desabou. Trata se da mesma relação não"-dialetizável que Anna
Akhmátova descobriu entre o cuco e os pássaros da
floresta\footnote{Agradeço a Gilberto Safra por meio de quem tomei conhecimento desse
poema de Akhmátova, numa conferência realizada na \versal{PUC}-\versal{SP} em 18.9.1999.} . Enquanto os passarinhos florestais
cantam apenas na dimensão luminosa, o pobre cuco transita entre a
claridade e a dimensão clandestina do longínquo. E~ele só anuncia o seu
anúncio ao cruzar a linha da fronteira. Sem lugar algum a não ser o do
trânsito pela zona fronteiriça, o cuco, esse animal mais morto, é,
paradoxalmente, o portador da palavra manifestante e da palavra mais
vulcânica. Há cucos de anúncio sóbrio e límpido, como Franz Kafka, o
grande cuco da Europa, de uma Europa cuja verdade ele próprio revelou,
um cuco, eu diria, do tamanho de um ``Big Ben'' encravado na Europa
Central, e um cuco que desconhecia sua própria ``cuquidade''. Há cucos
mais vulcânicos, cucos de língua russa como Dostoiévski, cujas notas do
subterrâneo ganham uma atualidade cada vez mais crescente, ou de língua
polaca, como o traiçoeiro Gombrowicz, um cuco zigue"-zagueante em fuga
permanente. Há, portanto, pelo planeta inteiro, uma pipocação cucológica
e seria realmente maravilhoso poder falar de cada um desses cucos, do
estilo e do jeito específico de cada um deles, comemorá"-los
amigavelmente, expondo"-nos ao lugar de onde falam. E~um tal trabalho não
seria mais um trabalho de crítica ou de teoria literária, pois crítica e
teoria literária, em geral, e infelizmente, apenas pacificam e exorcizam
o elemento intrinsecamente vulcânico da palavra reveladora e isso
acontece porque, em geral, os críticos literários, os especialistas em
literatura, enquanto criaturas em geral preenchidas e dependuradas na
identidade social de intelectual, transitam, apenas, a exemplo dos
pássaros de Akhmátova, de um lado da linha, o lado claro e metafísico,
desconhecendo inteiramente o dilaceramento e a situação clandestina,
hesitante e indecidível do cuco. E~por não terem notícia do lugar
exposto do cuco, os intelectuais se põem a interpretar poemas e
escrituras quando, na verdade, é o poema e a escritura que poderiam
interpretar e perfurar suas teorias estéticas, bem como sua propalada
intelectualidade. O~homem exposto, aninhado no enigma da medida real, é
ininterpretável, pois ele já se encontra na indigência da verdade e em
sua palavra o dito e o revelado já coincidem, não havendo mais material
algum a ser decifrado. O~homem teórico, entretanto, é ainda passível de
interpretação e de escavação hermenêutica precisamente porque, alérgico
ao lugar, refugiou"-se nalguma teoria sobre o mundo e sobre o
compartimento do assim chamado fenômeno estético. Mas não é o caso de
colocar aqui o mundo inteiro de ponta"-cabeças e mostrar isso de uma vez
por todas, mostrar que realmente vivemos num mundo às avessas e num
mundo que está de ponta cabeças e que seria necessário virá"-lo todo e
inteiro, a fim de que ele se torne, por um momento, verdadeiro e
respirável. Teorias literárias muitas vezes são defesas contra a
literatura, assim como teorias psicológicas são pequenas fobias diante
do terremoto humano. Elas passam e se sucedem na velocidade dos
automóveis, enquanto a literatura permanece e atravessa os séculos, de
tal modo que o próprio Marx, que pensava metafisicamente no sentido de
uma gênese social da literatura, reconheceu que ela transcendia
inteiramente essa gênese e que poderíamos, hoje, ler e amar algum poeta
grego antigo. Os paradigmas teóricos duram kuhnianamente o tempo que
dura a geração e a geração"-filhote, cujo discurso é o discurso poder na
empresa universitária e na empresa do jornalismo cultural, para a qual a
empresa universitária oferece a mão"-de"-obra qualificada. Os paradigmas
passam e mesmo agora, quando começam a chegar até a universidade o nome
dos cucolizadores da teoria literária, isto é, dos autores responsáveis
por uma cucolização filosófico"-literária tais como Derrida e Blanchot,
mas também Foucault e Deleuze, cujos trabalhos realmente importantes e
nutrientes digerem e lançam para trás ou para adiante as palavras do
imenso cuco filosófico do século \versal{XX}, o cuco Heidegger, cujo anúncio
sóbrio abriu a caixa preta que sustentava 25 séculos do gorgear e da
cantoria dos pássaros florestais; então, por intermédio do cuco
Heidegger, em cuja mesa de trabalho havia uma foto do cuco Dostoiévski,
ou por intermédio do cuco Gilles Deleuze que, embora negasse a
portinhola traseira da cuquidade e sua retração para a transcendência
finita, morou, entretanto, na linha sísmica da pura imanência, onde vida
e morte se confundem, e no plano em que Dentro e Fora se irmanam,
gerando a palavra de intensidade física; então, provavelmente haverá uma
mudança de discurso e muitos passarão a falar de cucos, de linhas e da
diferença, mas é necessário discriminar entre o falar de cucos, ou seja,
o falar sobre algo, e o morar em algo. Não se trata mais de falar e
discorrer sobre as coisas, de fingi"-las ou simulá"-las com o intelecto.
Quando mudamos de discurso, nada acontece além de uma auto"-inflação
intelectual e de um aumento de poder discursivo. A~urgência não está na
mudança de discurso, mas na alteração da nossa relação com a linguagem e
com a palavra. Por isso, muitas vezes conversamos com um especialista em
Dostoiévski ou com um especialista em Kafka e logo ficamos imensamente
tristes e decepcionados, pois percebemos que em sua história, em seu
corpo e em sua conversa não há o menor sinal de qualquer inquietação
kafkiana ou dostoiévskiana, e que ele, em sua víscera, desconhece
inteira e realmente aquilo sobre o que fala e que sua vida se desenrola
na antípoda da ideia que propaga. Ficamos tristes ao perceber que o
intelectual, escondido no jogo dos seus móbiles intelectuais, não sabe e
nem quer saber quem ele é e não pretende expor"-se ao vento fecundante
que destroçaria a figura intelectual enquanto esconderijo e esconderijo
poderoso. E~se falo dos intelectuais é apenas porque aponto para o
imenso risco da cooptação e neutralização universitárias da palavra do
cuco e da palavra vulcânica. Sabemos hoje, pois os vulcanólogos não se
cansam de repetir que, se não existissem os vulcões, a Terra seria lisa
como uma bola de gude ou uma bola de bilhar. Não haveria relevo nem
rugosidade, e muito menos sombra. E~isso é exatamente o que está
acontecendo hoje a todos nós, em todos os níveis, e isso é de uma
evidência cristalina, pois sentimos cada vez mais a dor e o frio da
luminosidade avassaladora, e o frio oriundo da trituração e do massacre
de pessoas e lugares nesta engrenagem inteiramente falsa e destrutiva
que chamamos de sistema ou de mundo normal. E é por isso que trabalho
numa autobiografia enquanto heterotanatografia; e nessa autobiografia eu
digo tudo e eu revelo tudo, esgotando e exaurindo a verdade do meu corpo
e do meu tempo, e mostro que se trata de um tempo em que a vida
verdadeira está ausente, de um tempo intrinsecamente sinistro e ainda
por cima pintado com as cores do bem; e eu aponto isso e mostro isso e
apenas isso, sem informar nada, pois já não se trata mais de informar
alguma coisa a alguém, e ao fazê"-lo, ao esgotar todo e qualquer segredo,
ao esgotar inteiramente o segredo de minha existência e de minha
cultura, ao colocar"-me inteiramente às claras, converto"-me no máximo
segredo e no passageiro clandestino, e isso tem de ser assim e não há
nada mais a fazer a não ser isso, a não ser nos lembrar de que somos um
fiapo visitado pelo mistério e que nossa vida é a sublevação hesitante
onde o mundo, em vão, se ilumina.
